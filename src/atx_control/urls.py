from django.urls import path

from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('power_short', views.power_short, name='power_short'),
        path('power_long', views.power_long, name='power_long'),
        path('reboot', views.reboot, name='reboot'),
]
