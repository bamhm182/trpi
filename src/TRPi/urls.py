from django.contrib import admin
from django.urls import include,path

urlpatterns = [
    path('', include('atx_control.urls')),
    path('admin/', admin.site.urls),
    path('atx/', include('atx_control.urls')),
]
