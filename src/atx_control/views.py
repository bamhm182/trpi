from django.shortcuts import render,redirect
from gpiozero import LED,Button
from time import sleep

def index(request):
    context = {
            "power_state": "Off" if check_pin(24) else "On",
            "hdd_state": "Inactive" if check_pin(22) else "Active"
    }
    return render(request, 'atx_control/index.html', context)

def power_short(request):
    press_pin(23, 0.5)
    return redirect('index')

def power_long(request):
    press_pin(23, 5)
    return redirect('index')

def reboot(request):
    press_pin(27, 0.5)
    return redirect('index')

def press_pin(number, length):
    pin = LED(number)
    pin.on()
    sleep(length)
    pin.off()
    pin.close()
    sleep(0.5)

def check_pin(number):
    pin = Button(number)
    state = pin.is_pressed
    pin.close()
    sleep(0.5)
    return state
