from django.apps import AppConfig


class AtxControlConfig(AppConfig):
    name = 'atx_control'
